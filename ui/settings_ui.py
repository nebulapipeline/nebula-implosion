# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'apps\implosion\ui\settings.ui'
#
# Created: Tue Feb 11 10:51:38 2020
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from Qt import QtCompat, QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(417, 237)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.mayaGroupBox = QtWidgets.QGroupBox(Dialog)
        self.mayaGroupBox.setObjectName("mayaGroupBox")
        self.gridLayout = QtWidgets.QGridLayout(self.mayaGroupBox)
        self.gridLayout.setSizeConstraint(QtWidgets.QLayout.SetMinimumSize)
        self.gridLayout.setObjectName("gridLayout")
        self.mayaVersionLabel = QtWidgets.QLabel(self.mayaGroupBox)
        self.mayaVersionLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.mayaVersionLabel.setObjectName("mayaVersionLabel")
        self.gridLayout.addWidget(self.mayaVersionLabel, 0, 0, 1, 1)
        self.mayaProjectLocationLabel = QtWidgets.QLabel(self.mayaGroupBox)
        self.mayaProjectLocationLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.mayaProjectLocationLabel.setObjectName("mayaProjectLocationLabel")
        self.gridLayout.addWidget(self.mayaProjectLocationLabel, 1, 0, 1, 1)
        self.lineEdit = QtWidgets.QLineEdit(self.mayaGroupBox)
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout.addWidget(self.lineEdit, 1, 1, 1, 1)
        self.mayaProjectBrowseLocation = QtWidgets.QToolButton(self.mayaGroupBox)
        self.mayaProjectBrowseLocation.setObjectName("mayaProjectBrowseLocation")
        self.gridLayout.addWidget(self.mayaProjectBrowseLocation, 1, 2, 1, 1)
        self.mayaVersionComboBox = QtWidgets.QComboBox(self.mayaGroupBox)
        self.mayaVersionComboBox.setObjectName("mayaVersionComboBox")
        self.gridLayout.addWidget(self.mayaVersionComboBox, 0, 1, 1, 1)
        self.gridLayout.setColumnMinimumWidth(0, 120)
        self.verticalLayout.addWidget(self.mayaGroupBox)
        self.archiveGroupBox = QtWidgets.QGroupBox(Dialog)
        self.archiveGroupBox.setFlat(False)
        self.archiveGroupBox.setObjectName("archiveGroupBox")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.archiveGroupBox)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.inputLocationLabel = QtWidgets.QLabel(self.archiveGroupBox)
        self.inputLocationLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.inputLocationLabel.setObjectName("inputLocationLabel")
        self.gridLayout_2.addWidget(self.inputLocationLabel, 0, 0, 1, 1)
        self.inputLocationLineEdit = QtWidgets.QLineEdit(self.archiveGroupBox)
        self.inputLocationLineEdit.setObjectName("inputLocationLineEdit")
        self.gridLayout_2.addWidget(self.inputLocationLineEdit, 0, 1, 1, 1)
        self.inputLocationBrowseButton = QtWidgets.QToolButton(self.archiveGroupBox)
        self.inputLocationBrowseButton.setObjectName("inputLocationBrowseButton")
        self.gridLayout_2.addWidget(self.inputLocationBrowseButton, 0, 2, 1, 1)
        self.outputLocationLabel = QtWidgets.QLabel(self.archiveGroupBox)
        self.outputLocationLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.outputLocationLabel.setObjectName("outputLocationLabel")
        self.gridLayout_2.addWidget(self.outputLocationLabel, 1, 0, 1, 1)
        self.outputLocationLineEdit = QtWidgets.QLineEdit(self.archiveGroupBox)
        self.outputLocationLineEdit.setObjectName("outputLocationLineEdit")
        self.gridLayout_2.addWidget(self.outputLocationLineEdit, 1, 1, 1, 1)
        self.outputLocationBrowseButton = QtWidgets.QToolButton(self.archiveGroupBox)
        self.outputLocationBrowseButton.setObjectName("outputLocationBrowseButton")
        self.gridLayout_2.addWidget(self.outputLocationBrowseButton, 1, 2, 1, 1)
        self.tempLocationLabel = QtWidgets.QLabel(self.archiveGroupBox)
        self.tempLocationLabel.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.tempLocationLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.tempLocationLabel.setObjectName("tempLocationLabel")
        self.gridLayout_2.addWidget(self.tempLocationLabel, 2, 0, 1, 1)
        self.tempLocationLineEdit = QtWidgets.QLineEdit(self.archiveGroupBox)
        self.tempLocationLineEdit.setObjectName("tempLocationLineEdit")
        self.gridLayout_2.addWidget(self.tempLocationLineEdit, 2, 1, 1, 1)
        self.tempLocationBrowseButton = QtWidgets.QToolButton(self.archiveGroupBox)
        self.tempLocationBrowseButton.setObjectName("tempLocationBrowseButton")
        self.gridLayout_2.addWidget(self.tempLocationBrowseButton, 2, 2, 1, 1)
        self.gridLayout_2.setColumnMinimumWidth(0, 120)
        self.verticalLayout.addWidget(self.archiveGroupBox)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtCompat.translate("Dialog", "Implosion Settings", None, -1))
        self.mayaGroupBox.setTitle(QtCompat.translate("Dialog", "Maya Settings", None, -1))
        self.mayaVersionLabel.setText(QtCompat.translate("Dialog", "Maya Version", None, -1))
        self.mayaProjectLocationLabel.setText(QtCompat.translate("Dialog", "Maya Project Location", None, -1))
        self.mayaProjectBrowseLocation.setText(QtCompat.translate("Dialog", "...", None, -1))
        self.archiveGroupBox.setTitle(QtCompat.translate("Dialog", "Archive Settings", None, -1))
        self.inputLocationLabel.setText(QtCompat.translate("Dialog", "Input Location", None, -1))
        self.inputLocationBrowseButton.setText(QtCompat.translate("Dialog", "...", None, -1))
        self.outputLocationLabel.setText(QtCompat.translate("Dialog", "Output Location", None, -1))
        self.outputLocationBrowseButton.setText(QtCompat.translate("Dialog", "...", None, -1))
        self.tempLocationLabel.setText(QtCompat.translate("Dialog", "Temp Location", None, -1))
        self.tempLocationBrowseButton.setText(QtCompat.translate("Dialog", "...", None, -1))

