import sys
from contextlib import contextmanager
from Qt import QtWidgets
from .main_window import MainWindow


ui = None


@contextmanager
def application():
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
        yield app
        app.exec_()
    else:
        yield app


def show():
    with application():
        ui = MainWindow()
        ui.show()
