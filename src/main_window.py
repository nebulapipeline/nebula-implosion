from __future__ import print_function

import os
import re
from zipfile import BadZipfile
from PySide2 import QtWidgets, QtCore, QtGui

from nebula.common import tools
from nebula.common import gui

from ..ui.main_ui import Ui_MainWindow


__base__ = os.path.dirname(os.path.dirname(__file__))
icons = os.path.join(__base__, 'icons')
html = os.path.join(__base__, 'html')


def logme(func):
    def _logme_wrapper(self, *args):
        print(func.__name__, end=' ')
        _args = ["index(%d, %d, %r)" % (
            arg.row(), arg.column(), arg.internalPointer(), )
                 if hasattr(arg, 'internalPointer') else arg
                 for arg in args]
        print(_args, end=' ')
        retval = func(self, *args)
        _retval = ("index(%d, %d, %r)" % (
            retval.row(), retval.column(), retval.internalPointer(), )
                   if hasattr(retval, 'internalPointer') else retval)
        print('--->>>', _retval)
        return retval
    return _logme_wrapper


class DropLabel(QtWidgets.QLabel):
    '''A label that shows an image and excepts a NebulaAssetTaskArchive file
    When it finds a file it emits the file_dropped signal
    '''
    file_dropped = QtCore.Signal(str)

    def __init__(self, parent=None):
        super(DropLabel, self).__init__(parent=parent)
        self._drop_label_html = None
        self.parent = parent
        self.setEnabled(True)
        self.setMinimumSize(QtCore.QSize(200, 200))
        self.setSizePolicy(QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed))
        self.setAcceptDrops(True)
        self.setFrameShape(QtWidgets.QFrame.WinPanel)
        self.setFrameShadow(QtWidgets.QFrame.Plain)
        self.setText(self.drop_label_html)
        self.setTextFormat(QtCore.Qt.AutoText)
        self.setPixmap(QtGui.QPixmap(os.path.join(icons, 'drop_file.svg')))
        self.setAlignment(QtCore.Qt.AlignCenter)
        self.setObjectName('dropLabel')

    @property
    def drop_label_html(self):
        '''Retrieve the drop label html for an external file'''
        if self._drop_label_html is None:
            html_file = os.path.join(html, 'drop_label.html')
            with open(html_file) as _file:
                self._drop_label_html = _file.read()
        return self._drop_label_html

    def dropped_zip(self, urls):
        '''Given a set of dropped file urls selected a dropped zip'''
        for url in urls:
            local = url.toLocalFile()
            if os.path.splitext(local)[1].lower() == ".zip":
                return local
        return False

    def dragEnterEvent(self, event):
        ''':type event: PySide2.QtGui.QDragEnterEvent'''
        mimeData = event.mimeData()
        if mimeData.hasUrls() and self.dropped_zip(mimeData.urls()):
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        ''':type event: PySide2.QtGui.QDragMoveEvent'''
        mimeData = event.mimeData()
        if mimeData.hasUrls() and self.dropped_zip(mimeData.urls()):
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        ''':type event: PySide2.QtGui.QDropEvent'''
        mimeData = event.mimeData()

        if mimeData.hasUrls():
            dropped_zip = self.dropped_zip(mimeData.urls())
            self.file_dropped.emit(dropped_zip)
            event.accept()
        else:
            event.ignore()

    def mouseDoubleClickEvent(self, event):
        self.parent.browseForFile()


def get_nice_name(name):
    ''' Return a titlecased space separated name from underscore separated name

    >>> get_nice_name('hello_world')
    Hello World

    :type name: name
    '''
    return ' '.join(x.capitalize() for x in name.split('_'))


class AssetTaskArchiveModel(QtCore.QAbstractItemModel):
    '''Model for feeding content of an asset task archive to a QTreeView'''

    asset_file_re = re.compile(r'([^\/]*)\/(.*)\/(.*?)$')

    def __init__(self, parent, file_path):
        super(AssetTaskArchiveModel, self).__init__(parent)
        self.setFilePath(file_path)
        self.arch = None
        self.open()

    def setFilePath(self, path):
        ''':type path: str'''
        self.file_path = path

    def reset_cache(self):
        ''':type path: str'''
        self._assets = None
        self._asset_contexts = None
        self._context_files = None

    def open(self):
        '''Open the file set by setFilePath, this function is called on init'''
        self.beginResetModel()
        self.arch = tools.AssetTaskArchive(self.file_path)
        self.reset_cache()
        self.endResetModel()

    def get_filename(self, index):
        if index.isValid():
            data = index.internalPointer()
            if isinstance(data, tuple):
                return '/'.join(data) + '/'
            elif '/' in data:
                return data
            else:
                return data if data.endswith('/') else data + '/'
        return ''

    def get_info(self, index):
        return self.arch.NameToInfo[self.get_filename(index)]

    def close(self):
        '''Close the archive and release'''
        self.arch.close()

    def get_assets(self):
        '''Get list of all assets packed in the archive'''
        if self._assets is None:
            self._assets = self.arch.list_assets()
        return self._assets

    def get_asset_contexts(self, asset):
        '''Get list of all contexts listed under an asset'''
        if self._asset_contexts is None:
            self._asset_contexts = {}
        asset_contexts = self._asset_contexts.get(asset)

        # store asset, context tuples in cache
        if asset_contexts is None:
            self._asset_contexts[asset] = asset_contexts = [
                    (asset, context)
                    for context in self.arch.list_asset_contexts(asset)]

        return asset_contexts

    def get_context_files(self, asset_context):
        '''Get list of all files listed in the archive under a given asset
        context'''
        if self._context_files is None:
            self._context_files = {}
        context_files = self._context_files.get(asset_context)
        if context_files is None:
            self._context_files[asset_context] = context_files = \
                    self.arch.list_context_files(*asset_context)
        return context_files

    def get_task_info(self):
        '''Return the task info retrieved from the archive'''
        return self.arch.task_info

    def rowCount(self, parent=QtCore.QModelIndex()):
        '''Return the rowCount under the given parent index'''
        if parent.isValid():
            parent_info = parent.internalPointer()
            num = 0
            if isinstance(parent_info, tuple):  # asset context
                num = len(self.get_context_files(parent_info))
            elif '/' not in parent_info:        # asset
                num = len(self.get_asset_contexts(parent_info))
            return num
        return len(self.arch.list_assets())

    def columnCount(self, parentIndex=QtCore.QModelIndex()):
        return 1

    def headerData(self, index, direction, role):
        if role == QtCore.Qt.DisplayRole:
            return 'Archive Content'

    def index(self, row, column, parent=QtCore.QModelIndex()):
        '''Return an index '''
        if parent.isValid():
            parent_info = parent.internalPointer()
            if isinstance(parent_info, tuple):
                int_data = self.get_context_files(parent_info)[row]
            else:
                int_data = self.get_asset_contexts(parent_info)[row]
            return self.createIndex(row, 0, int_data)
        return self.createIndex(row, 0, self.get_assets()[row])

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.DisplayRole:
            internal_data = index.internalPointer()
            if isinstance(internal_data, tuple):
                return internal_data[-1]
            return internal_data

    def parent(self, index):
        '''Return the parent index of the given index'''
        if index.isValid():
            data = index.internalPointer()

            if isinstance(data, tuple):   # Instance
                row = self.get_assets().index(data[0])
                return self.createIndex(row, 0, self.get_assets()[row])

            elif '/' in data:
                match = self.asset_file_re.match(data)
                if match:
                    asset, context, filename = match.groups()
                    asset_contexts = self.get_asset_contexts(asset)
                    row = asset_contexts.index((asset, context))
                    return self.createIndex(row, 0, asset_contexts[row])

        # parent is invalid if index is asset or invalid
        return QtCore.QModelIndex()


class ScrollFormFrame(QtWidgets.QFrame):
    def __init__(self, parent=None):
        super(ScrollFormFrame, self).__init__(parent=parent)
        self.setMinimumSize(100, 100)
        self.scroll = QtWidgets.QScrollArea(self)
        self.scroll.setWidgetResizable(True)
        self.scrollContentWidget = QtWidgets.QWidget()
        self.scrollContentWidgetLayout = QtWidgets.QFormLayout(
                self.scrollContentWidget)
        self.scroll.setWidget(self.scrollContentWidget)

    def addRow(self, thing_one, thing_two):
        return self.scrollContentWidgetLayout.addRow(thing_one, thing_two)

    def addWidget(self, widget):
        return self.scrollContentWidgetLayout.addWidget(widget)

    def clear(self):
        for i in reversed(range(self.scrollContentWidgetLayout.count())):
            widget = self.scrollContentWidgetLayout.itemAt(i).widget()
            widget.setParent(None)
            widget.deleteLater()

    def resizeEvent(self, event):
        ''':type event: QtGui.QResizeEvent'''
        self.scroll.setFixedSize(event.size())
        event.accept()


class TaskInfoFrame(QtWidgets.QFrame):
    '''UI for showing TaskInfo for an archive.
    It contains a form on top to show task related info.
    On the bottom it shows contents of the task archive and information related
    to selected files from the archive.
    '''

    def __init__(self, parent=None):
        super(TaskInfoFrame, self).__init__(parent=parent)
        self.layout = QtWidgets.QHBoxLayout(self)
        self.vsplit = QtWidgets.QSplitter(QtCore.Qt.Vertical)
        self.layout.addWidget(self.vsplit)

        # top frame to show the task info
        self.formFrame = ScrollFormFrame(self)
        self.hsplit = QtWidgets.QSplitter(QtCore.Qt.Horizontal)

        # bottom splits to show the contents of the archive
        self.treeView = QtWidgets.QTreeView(self.hsplit)
        self.treeView.setSelectionMode(
                QtWidgets.QAbstractItemView.ContiguousSelection)
        self._createPropView()
        self.hsplit.addWidget(self.treeView)
        self.hsplit.addWidget(self.propView)
        self.hsplit.setSizes([100, 100])

        self.vsplit.addWidget(self.formFrame)
        self.vsplit.addWidget(self.hsplit)
        self.vsplit.setSizes([140, 280])

        self.model = None

    def _createPropView(self):
        self.propView = QtWidgets.QFrame(self.hsplit)
        self._propScrollArea = QtWidgets.QScrollArea(self.propView)
        self._propScrollWidget = QtWidgets.QWidget()
        self.propLayout = QtWidgets.QVBoxLayout(self._propScrollWidget)

    def _clearPropView(self):
        for i in reversed(range(self.propLayout.count())):
            widget = self.propLayout.itemAt(i).widget()
            if isinstance(widget, QtWidgets.QFrame):
                widget.setParent(None)
                widget.deleteLater()

    def setFilePath(self, file_path):
        '''Set the Task file path and reset the model'''
        if self.model:
            self.model.close()
        self.model = AssetTaskArchiveModel(self, file_path)
        self.treeView.setModel(self.model)
        self.treeView.expandAll()
        self.selectionModel = self.treeView.selectionModel()
        self.selectionModel.selectionChanged.connect(self.selectionChanged)
        self._setTaskInfo(self.model.arch.task_info)

    def selectionChanged(self, selected, deselected):
        self._clearPropView()

        for index in self.selectionModel.selectedIndexes():
            info = self.model.get_info(index)
            infoFrame = self._createArchivedFileInfo(info)
            self.propLayout.addWidget(infoFrame)

    def _setTaskInfo(self, taskInfo):
        '''Fill the form'''
        self.formFrame.clear()

        for name in taskInfo.fields:
            label = QtWidgets.QLabel(get_nice_name(name) + ":")
            lineEdit = QtWidgets.QLineEdit(self)
            lineEdit.setText(getattr(taskInfo, name))
            lineEdit.setEnabled(False)
            lineEdit.setFrame(False)
            self.formFrame.addRow(label, lineEdit)

        self.formButton = QtWidgets.QPushButton("Copy Task Info")
        self.formButton.clicked.connect(self.copyTaskInfo)
        self.formFrame.addWidget(self.formButton)

    def _createArchivedFileInfo(self, fileInfo):
        propFrame = ScrollFormFrame(self)

        for name in fileInfo.__slots__:
            label = QtWidgets.QLabel(get_nice_name(name))
            lineEdit = QtWidgets.QLineEdit(self)
            lineEdit.setText(str(getattr(fileInfo, name)))
            lineEdit.setEnabled(False)
            lineEdit.setFrame(False)
            propFrame.addRow(label, lineEdit)

        self.formButton = QtWidgets.QPushButton('Extract')
        self.formButton.clicked.connect(lambda: self.extractFile(fileInfo))
        propFrame.addWidget(self.formButton)

        return propFrame

    def extractFile(self, name):
        print('extract file', name)

    def copyTaskInfo(self):
        json = self.model.arch.task_info.dumps()
        QtWidgets.QApplication.clipboard().setText(json)


class DropLabelFrame(QtWidgets.QFrame):
    file_dropped = QtCore.Signal(str)

    def __init__(self, parent=None):
        '''A Frame with a DropLabel down the center of it that fires when
        something is dropped in the lable'''
        super(DropLabelFrame, self).__init__(parent)
        self.layout = QtWidgets.QVBoxLayout(self)
        self.dropLabel = DropLabel(self)
        self.dropLabel.file_dropped.connect(self.file_dropped)
        self.layout.addWidget(self.dropLabel)
        self.layout.setAlignment(QtCore.Qt.AlignHCenter)


class MainWindow(Ui_MainWindow, QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent=parent)
        self._parent = parent
        self.current_task_file = None
        self.taskInfoUI = None
        self.dropLabel = None
        self.setupUi(self)

        self.taskTabLayout = self.taskTab.layout()
        self.stackedLayout = QtWidgets.QStackedLayout(self.taskTabLayout)

        self.actionOpen_Job.triggered.connect(self.browseForFile)
        self.actionClear.triggered.connect(self.close_file)

        self.nextButton = self.commandLinkButton
        self.nextButton.clicked.connect(self.next)

        self.setupDropLabel()
        self.showDropLabel()
        self.setupTaskInfo()

    def setupDropLabel(self):
        self.dropLabelFrame = DropLabelFrame(self)
        self.dropLabelFrame.file_dropped.connect(self.open_file)
        self.stackedLayout.addWidget(self.dropLabelFrame)

    def showDropLabel(self):
        self.setTabsEnabled(False)
        self.mainTabWidget.setCurrentIndex(0)
        self.stackedLayout.setCurrentIndex(0)

    def setupTaskInfo(self):
        self.taskInfoFrame = TaskInfoFrame()
        self.stackedLayout.addWidget(self.taskInfoFrame)

    def showTaskInfoFrame(self, taskInfo):
        self.setTabsEnabled(True)
        self.taskInfoFrame.setFilePath(self.current_task_file)
        self.stackedLayout.setCurrentIndex(1)
        self.nextButton.setEnabled(True)

    def setTabsEnabled(self, enabled):
        for idx in range(1, self.mainTabWidget.count()):
            self.mainTabWidget.setTabEnabled(idx, enabled)

    def open_archive(self, filename):
        with tools.AssetTaskArchive(filename, mode='r') as arch:
            self.statusbar.showMessage('Validating archive ...')
            if arch.check_validate():
                self.current_task_file = filename

    def open_file(self, filename):
        try:
            self.open_archive(filename)

        except (tools.FailedIntegrityCheck, BadZipfile,
                KeyError, ValueError) as exc:
            self.statusbar.showMessage('Invalid Task Archive Found', 2000)
            gui.showMessage(
                    self,
                    msg="Invalid Task Archive '%s'" % (
                        os.path.basename(filename)),
                    details="%s: %s" % (type(exc), exc.args[0]),
                    icon=QtWidgets.QMessageBox.Critical)

        else:
            self.statusbar.showMessage(
                    "Opened '%s'" % self.current_task_file, 2000)
            self.showTaskInfoFrame(self.current_task_file)
            self.setTabsEnabled(True)

    def close_file(self):
        self.current_task_file = None
        self.showDropLabel()

    def browseForFile(self):
        filename, _ = QtWidgets.QFileDialog.getOpenFileName(
                parent=self, caption='Select Nebula Task Archive zip',
                filter='Nebula Archives (*.zip)')
        if filename:
            self.open_file(filename)

    def next(self):
        self.mainTabWidget.setCurrentIndex(
                (self.mainTabWidget.currentIndex() + 1) %
                self.mainTabWidget.count())
